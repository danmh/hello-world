---
title: I think I did it
subtitle: A short story about Hugo
date: 2022-02-04
tags: ["firstpost", "experiment"]
---

# So I did it...

After a small hicup where I needed to get access to GitLab Ultimate I finally deployed and released my first GitLab Pages website.

I'm not sure if I'm ever going to use this again. But it has been a fun experiement and incredibly easy to get up and running.

I can see why its popular. I often wondered why Jenko opted for something like this. But having tried it, it makes perfect sense now.

I'm enjoying Markdown more thank I thought I would.

## My desk setup

So my monitor wobbles more thank I was hoping it would. I'm not sure if there is anything I can do about that. My table is small and feeble so that doesn't help at all.

## Conclusion

Thanks for coming to my TED talk and `HELLO WORLD`!
